﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    public GameObject explosionObj;
    public float velocity;

    [HideInInspector]
    public bool shooting;

    private GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        prefab = Resources.Load("Prefabs/Projectile") as GameObject;
        shooting = false;        
    }

    // Update is called once per frame
    void Update() {}

    public void Shoot(float intensity)
    {
        shooting = true;
        intensity = intensity / 2;
        if (intensity > 1.2f) intensity = 1.2f;

        

        var rotY = this.transform.rotation.y * 2.704529785f;

        explosionObj.GetComponent<ParticleSystem>().Play();

        GameObject projectile = Instantiate(prefab) as GameObject;  
        projectile.transform.position = new Vector3(
                        this.transform.position.x + rotY, 
                        this.transform.position.y + 0.94f, 
                        this.transform.position.z + 1.4f - (rotY / 4)
        );

        Rigidbody rb = projectile.GetComponent<Rigidbody>();

        Vector3 moveVector = this.transform.forward;
        moveVector.y = 0.3f;
        rb.AddForce(moveVector * intensity * velocity, ForceMode.Impulse);

        StartCoroutine(WaitForExplosion());
    }

    public void RotateCannon(Vector3 rotation)
    {
        this.transform.Rotate(rotation);
        float deg = this.transform.rotation.eulerAngles[1];
        //if (deg > 50) deg = (360 - deg) * -1;
        float deg2 = deg * Mathf.Deg2Rad;
        explosionObj.GetComponent<ParticleSystem>().startRotation = deg2 + 0.3f;     
    }

    private IEnumerator WaitForExplosion()
    {
        yield return new WaitForSeconds(1.3f);
        shooting = false;
    }
}
