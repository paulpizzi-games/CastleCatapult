﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class UIController : MonoBehaviour
{
    public GameObject infoFields;
    public GameObject countdownContainer;
    public GameObject GameOverScreen;
    public Text rotVal;
    public Text shotsVal;
    public Text timerVal;
    public Text countdownVal;

    private float time = 30.49f;
    private float countdown = 3.5f;

    private bool preGame;

    // Start is called before the first frame update
    void Start()
    {
        preGame = true;
        countdownVal.text = countdown.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (preGame)
        {
            countdown -= Time.deltaTime;
            countdownVal.text = Mathf.Round(countdown).ToString();
            if (countdown <= 1)
            {
                infoFields.SetActive(true);
                countdownVal.text = "GO!";
                timerVal.text = time.ToString();
                StartCoroutine(DisplayGO());
                preGame = false;
            }
        }
        else
        {
            time -= Time.deltaTime;
            timerVal.text = Mathf.Round(time).ToString();
            if (time <= 0)
            {
                Time.timeScale = 0;
                GameOverScreen.SetActive(true);
            }

        }
        
    }

    private IEnumerator DisplayGO()
    {
        yield return new WaitForSeconds(1);
        countdownContainer.SetActive(false);

    }

    public void SetRotVal(Quaternion val)
    {
        float deg = val.eulerAngles[1];
        if (deg > 50) deg = (360 - deg) * -1;
        rotVal.text = Mathf.Round(deg).ToString();
    }

    public void SetShotsVal(int val)
    {
        float oldVal = float.Parse(shotsVal.text);
        shotsVal.text = (oldVal + val).ToString();
    }

}
