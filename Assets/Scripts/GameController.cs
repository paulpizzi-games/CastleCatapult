﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public CannonController cannon;
    public WallController wall;
    public UIController UI;

    private float pressTime;

    private Vector2 startTouchPos;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {

        //==========================\\
        //===== Shooting Input KEYBOARD =====\\
        //==========================\\
        //if (Input.GetMouseButtonDown(0))
        //{
        //    pressTime = Time.time;
        //}
        //if (Input.GetMouseButtonUp(0) && !cannon.shooting)
        //{
        //    cannon.Shoot(Time.time - pressTime);
        //    UI.SetShotsVal(1);
        //}

        //==========================\\
        //===== Shooting Input TOUCH =====\\
        //==========================\\
        if (Input.touchCount == 2) {
            if (Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(1).phase == TouchPhase.Began)
            {
                pressTime = Time.time;
            }
            
            if(Input.GetTouch(0).phase == TouchPhase.Ended && Input.GetTouch(1).phase == TouchPhase.Ended)
            {
                cannon.Shoot(Time.time - pressTime);
                UI.SetShotsVal(1);
            }
        }

        //==========================\\
        //===== Rotation Input TOUCH =====\\
        //==========================\\
        if (Input.touchCount == 1)
        {
            Touch t = Input.GetTouch(0);
                    
            switch (t.phase)
            {
                case TouchPhase.Began:
                    startTouchPos = t.position;
                    break;

                case TouchPhase.Moved:
                    if (t.position.x < startTouchPos.x && cannon.transform.rotation.y > -0.38f)
                    {
                        cannon.RotateCannon(new Vector3(0, -50 * Time.deltaTime, 0));
                        UI.SetRotVal(cannon.transform.rotation);
                    }
                    else if (t.position.x >= startTouchPos.x && cannon.transform.rotation.y < 0.38f)
                    {
                        cannon.RotateCannon(new Vector3(0, 50 * Time.deltaTime, 0));
                        UI.SetRotVal(cannon.transform.rotation);
                    }
                    break;
            }
        }

        //==========================\\
        //===== Rotation Input KEYBOARD =====\\
        //==========================\\
        if (Input.GetKey(KeyCode.LeftArrow) && cannon.transform.rotation.y > -0.38f)
        {
            //left
            cannon.RotateCannon(new Vector3(0, -50 * Time.deltaTime, 0));
            UI.SetRotVal(cannon.transform.rotation);
        }
        if (Input.GetKey(KeyCode.RightArrow) && cannon.transform.rotation.y < 0.38f)
        {
            //right
            cannon.RotateCannon(new Vector3(0, 50 * Time.deltaTime, 0));
            UI.SetRotVal(cannon.transform.rotation);
        }


    }
}
