﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WallController : MonoBehaviour
{
    private SimpleMeshExploder simpleMeshExploder;
    private GameObject prefab;


    // Start is called before the first frame update
    void Start()
    {
        prefab = Resources.Load("Prefabs/HitPoint") as GameObject;
        simpleMeshExploder = FindObjectOfType<SimpleMeshExploder>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag.Equals("projectile"))
        {
            collision.collider.attachedRigidbody.velocity = new Vector3(0, 0, 0);

            //GameObject hitPoint = Instantiate(prefab) as GameObject;
            //hitPoint.transform.position = prefab.transform.position;
            //hitPoint.transform.position = collision.collider.transform.position;

           // hitPoint.transform.position = collision.collider.transform.position;

            if (this.transform.CompareTag("Explodable") || this.transform.CompareTag("Explodable_floor")||
                this.transform.CompareTag("Explodable_Piece"))
            {
                simpleMeshExploder.Explode(this.transform);
            }

            StartCoroutine(WaitForDestroy(collision.collider.gameObject));
        }

        if (collision.collider.gameObject.tag.Equals("floor"))
        {
            
            
            if (this.transform.CompareTag("Explodable"))
            {
                Debug.Log("Wall hit floor");
                simpleMeshExploder.Explode(this.transform);
            }

            //StartCoroutine(WaitForDestroy(collision.collider.gameObject));
        }
    }

    IEnumerator WaitForDestroy(GameObject obj)
    {
        yield return new WaitForSeconds(3.0f);
        Destroy(obj);
        Debug.Log("destroyed");
    }
}
